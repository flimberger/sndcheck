/*-
 * Copyright (c) 2024 Florian Limberger
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#ifndef SNDCHK_H_
#define SNDCHK_H_

#define SNDCHK_NCHAN	2
#define SNDCHK_SRATE	11025

enum output_mode {
	OUTPUT_BOTH,
	OUTPUT_LEFT,
	OUTPUT_RIGHT,
};

void	fillbuf(uint8_t *, size_t, float);

#endif /* SNDCHK_H_ */
