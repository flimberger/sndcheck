#include <alsa/asoundlib.h>

#include <sys/types.h>

#include <err.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>
#include <unistd.h>

#include "sndchk.h"

static snd_pcm_t *
alsa_open(enum output_mode outmode)
{
	snd_pcm_hw_params_t	*hwparams;

	if (snd_pcm_open(&device, "default", SND_PCM_STREAM_PLAYBACK, 0) < 0)
		err(EX_OSERR, "failed to open default sound device");
	snd_pcm_hw_params_alloca(&hwparams);
	snd_pcm_hw_params_any(device, hwparams);
	snd_pcm_hw_params_set_access(device, hwparams,
	    SND_PCM_ACCESS_RW_INTERLEAVED);
	snd_pcm_hw_params_set_format(device, hwparams, SND_PCM_FORMAT_U8);
	snd_pcm_hw_params_set_channels(device, hwparams, SNDCHK_NCHAN);
	snd_pcm_hw_params_set_rate(device, hwparams, SNDCHK_SRATE, 0);
	snd_pcm_hw_params_set_periods(device, hwparams, 10, 0);
	snd_pcm_hw_params_set_periods(device, hwparams, 10000, 0);
	snd_pcm_hw_params(device, hwparams);
}

int
main(int argc, char *argv[])
{
	snd_pcm_t	*device;
	uint8_t		*buf, *p;
	ssize_t		 res;
	enum output_mode outmode;
	int		 c, dur, n;
	float		 dt;

	dur = 2;
	outmode = OUTPUT_BOTH;
	while ((c = getopt(argc, argv, "d:lr")) != -1) {
		switch (c) {
		case 'd':
			dur = strtol(optarg, NULL, 10);
			break;
		case 'l':
			outmode = OUTPUT_LEFT;
			break;
		case 'r':
			outmode = OUTPUT_RIGHT;
			break;
		default:
			fprintf(stderr,
			    "usage: sndcheck [-lr] [-d DURATION]\n");
		}
	}
	argc -= optind;
	argv += optind;

	if ((dur <= 0) || (dur > 60))
		errx(EX_USAGE, "invalid duration value specified");

	device = alsa_open(outmode);
	n = SNDCHK_SRATE * dur;
	buf = calloc(n * SNDCHK_NCHAN, 1);
	if (!buf)
		err(EX_OSERR, "allocation failed");
	dt = 1.f / SNDCHK_SRATE;
	fillbuf(buf, n, dt);
	p = buf;
	while (n > 0) {
		if ((res = snd_pcm_writei(device, p, n)) < 0) {
			err(EX_IOERR, "write failed");
		}
		n -= res;
		p += res;
	}
	snd_pcm_drain(device);
	snd_pcm_close(device);
	free(buf);
}
