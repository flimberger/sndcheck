PROG=	sndchk
MAN=	sndchk.1

SRCS=	freebsd_sndchk.c fillbuf.c

LDADD=	-lm

WARNS=	6

${SRCS}:	sndchk.h

.include <bsd.prog.mk>
