/*-
 * Copyright (c) 2023-2024 Florian Limberger
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <math.h>
#include <stddef.h>
#include <stdint.h>

#include "sndchk.h"

void
fillbuf(uint8_t *p, size_t n, float dt)
{
	float	v;
	uint8_t	b;

	for (size_t i = 0; i < n; i++) {
		v = sinf(440 * i * dt * M_PI * 2);
		b = (uint8_t)roundf(v * 127.f + 128.f);
		*p++ = b;
		*p++ = b;
	}
}
