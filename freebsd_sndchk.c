/*-
 * Copyright (c) 2023-2024 Florian Limberger
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/soundcard.h>

#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>
#include <unistd.h>

#include "sndchk.h"

static void
sndsetparam(int fd, int param, int value, const char *msg)
{
	int	feedback;

	feedback = value;
	if (ioctl(fd, param, &feedback) < 0)
		errx(EX_IOERR, "failed to configure device: %s", msg);
	if (feedback != value)
		errx(EX_OSERR, "unsupported configuration: %s", msg);
}

static int
oss_open(enum output_mode outmode)
{
	int	fd, vol;

	if ((fd = open("/dev/dsp", O_WRONLY)) < 0)
		errx(EX_OSFILE, "failed to open /dev/dsp");
	sndsetparam(fd, SNDCTL_DSP_SETFMT, AFMT_U8, "audio format U8");
	sndsetparam(fd, SNDCTL_DSP_CHANNELS, SNDCHK_NCHAN, "2 channels");
	sndsetparam(fd, SNDCTL_DSP_SPEED, SNDCHK_SRATE, "sample rate 11.025 Hz");

	if (outmode != OUTPUT_BOTH) {
		if (ioctl(fd, MIXER_READ(SOUND_MIXER_PCM), &vol) < 0) {
			warn("failed to read PCM mixer volume");
			return fd;
		}
		if (outmode == OUTPUT_LEFT)
			vol = vol & 0x00FF;
		else /* outmode == OUTPUT_RIGHT */
			vol = vol & 0xFF00;
		if (ioctl(fd, MIXER_WRITE(SOUND_MIXER_PCM), &vol) < 0)
			warn("failed to write PCM volume");
	}

	return fd;
}

int
main(int argc, char *argv[])
{
	uint8_t	*buf, *p;
	ssize_t	 res;
	enum output_mode outmode;
	int	 c, dur, fd, n;
	float	 dt;

	dur = 2;
	outmode = OUTPUT_BOTH;
	while ((c = getopt(argc, argv, "d:lr")) != -1) {
		switch (c) {
		case 'd':
			dur = strtol(optarg, NULL, 10);
			break;
		case 'l':
			outmode = OUTPUT_LEFT;
			break;
		case 'r':
			outmode = OUTPUT_RIGHT;
			break;
		default:
			fprintf(stderr,
			    "usage: sndcheck [-lr] [-d DURATION]\n");
		}
	}
	argc -= optind;
	argv += optind;

	if ((dur <= 0) || (dur > 60))
		errx(EX_USAGE, "invalid duration value specified");

	fd = oss_open(outmode);
	n = SNDCHK_SRATE * dur;
	buf = calloc(n * SNDCHK_NCHAN, 1);
	if (!buf)
		err(EX_OSERR, "allocation failed");
	dt = 1.f / SNDCHK_SRATE;
	fillbuf(buf, n, dt);
	p = buf;
	while (n > 0) {
		if ((res = write(fd, p, n)) < 0) {
			err(EX_IOERR, "write failed");
		}
		n -= res;
		p += res;
	}
	close(fd);
	free(buf);
}
